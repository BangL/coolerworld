﻿using UnityEngine;

namespace Assets.Scripts {

    public static class Extensions {

        public static Vector3 Round(this Vector3 v, float step = 1f) {
            return new Vector3(
                Mathf.Round(v.x / step) * step,
                Mathf.Round(v.y / step) * step,
                Mathf.Round(v.z / step) * step
            );
        }

        public static bool IsNear(this Vector3 v, Vector3 target, float distance) {
            return Mathf.Abs(v.x - target.x) < distance
                && Mathf.Abs(v.y - target.y) < distance
                && Mathf.Abs(v.z - target.z) < distance;
        }

        public static Quaternion Round(this Quaternion v, float step = 90f) {
            return Quaternion.Euler(v.eulerAngles.Round(step));
        }
    }
}