//using Assets.Scripts.Kula.States;
//using UnityEngine;
//using StateMachine = Assets.Scripts.Core.StateMachine;

//namespace Assets.Scripts.Kula {
//    public class KulaController : MonoBehaviour {
//        public static KulaController Instance { get; private set; }

//        public Transform Hull;

//        public StateMachine StateMachine = new();
//        public StandingState StandingState;
//        public SnapState SnapState;

//        //public TurningState TurningState;
//        public RollingState RollingState;

//        // public GravityShiftingState GravityShiftingState;
//        //public JumpingUpState JumpingUpState;
//        //public JumpingForwardState JumpingForwardState;
//        //public FallingState FallingState;

//        private KulaInput.KulaControlsActions controls;

//        public Vector3? TargetPosition { get; set; } = null;

//        private void Awake() {
//            if (Instance != null)
//                Destroy(Instance.gameObject);
//            Instance = this;
//        }

//        private void Start() {
//            // init controls
//            controls = new KulaInput().KulaControls;
//            controls.Enable();

//            StandingState = new StandingState(this, StateMachine);
//            // TurningState = new TurningState(this, StateMachine);
//            RollingState = new RollingState(this, StateMachine);
//            SnapState = new SnapState(this, StateMachine);
//            //GravityShiftingState = new GravityShiftingState(this, StateMachine);
//            //JumpingUpState = new JumpingUpState(this, StateMachine);
//            //JumpingForwardState = new JumpingForwardState(this, StateMachine);
//            //FallingState = new FallingState(this, StateMachine);

//            // StateMachine.RegisterTransition(StandingState, FallingState, () => false);
//            StateMachine.RegisterTransition(StandingState, RollingState, () =>
//                 controls.RollForward.triggered);

//            StateMachine.RegisterTransition(RollingState, SnapState, () =>
//                TargetPosition.HasValue && TargetPosition.Value.IsNear(transform.position, .5f));

//            //StateMachine.RegisterTransition(StandingState, JumpingUpState, () => false);
//            //StateMachine.RegisterTransition(StandingState, JumpingForwardState, () => false);
//            //StateMachine.RegisterTransition(StandingState, TurningState, () => false);
//            //StateMachine.RegisterTransition(StandingState, GravityShiftingState, () => false);

//            //StateMachine.RegisterTransition(RollingState, GravityShiftingState, () => false);
//            //StateMachine.RegisterTransition(RollingState, FallingState, () => false);
//            StateMachine.RegisterTransition(SnapState, StandingState, () =>
//                TargetPosition.HasValue && TargetPosition.Value.IsNear(transform.position, .05f));

//            //StateMachine.RegisterTransition(RollingState, JumpingForwardState, () => false);

//            //StateMachine.RegisterTransition(GravityShiftingState, StandingState, () => false);
//            //StateMachine.RegisterTransition(JumpingUpState, StandingState, () => false);
//            //StateMachine.RegisterTransition(JumpingForwardState, StandingState, () => false);
//            //StateMachine.RegisterTransition(TurningState, StandingState, () => false);

//            StateMachine.Initialize(StandingState);
//        }

//        private void Update() {
//            StateMachine.OnUpdate();
//            StateMachine.CurrentState.Update();
//        }

//        private void FixedUpdate() {
//            StateMachine.CurrentState.FixedUpdate();
//        }
//    }
//}