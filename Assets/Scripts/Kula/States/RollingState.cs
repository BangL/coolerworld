﻿//using Assets.Scripts.Core;
//using UnityEngine;

//namespace Assets.Scripts.Kula.States {
//    public class RollingState : GroundedState {
//        public RollingState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
//        }

//        public override void Enter() {
//            base.Enter();

//            // get current position
//            var transform = KulaController.transform;

//            // calculate and set target position
//            KulaController.TargetPosition =
//                (transform.position + transform.forward).Round() + (TweakData.BALL_OFFSET * transform.up);

//            // start rolling
//            Rigidbody.AddForce(transform.forward * 2, ForceMode.Impulse);
//        }

//        public override void Exit() {
//            base.Exit();
//        }
//    }
//}