﻿using Assets.Scripts.Core;

namespace Assets.Scripts.Kula.States {

    public class FallingState : StateBase {

        public FallingState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
        }

        public override void Exit() {
            base.Exit();
        }
    }
}