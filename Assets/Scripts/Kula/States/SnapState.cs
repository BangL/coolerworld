﻿//using Assets.Scripts.Core;
//using UnityEngine;

//namespace Assets.Scripts.Kula.States {
//    public class SnapState : GroundedState {
//        private Vector3 startPosition;
//        private float time;

//        public SnapState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
//        }

//        public override void Enter() {
//            base.Enter();
//            startPosition = KulaController.transform.position;
//            time = 0f;
//        }

//        public override void Update() {
//            base.Update();
//            time += Time.deltaTime / .2f;

//            if (time > 1f) time = 1f;

//            Vector3 currentPosition =
//                Vector3.Lerp(startPosition, KulaController.TargetPosition.Value, time);

//            Rigidbody.MovePosition(currentPosition);
//        }

//        public override void Exit() {
//            base.Exit();
//        }
//    }
//}