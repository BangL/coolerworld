﻿using Assets.Scripts.Core;

namespace Assets.Scripts.Kula.States {

    public class JumpingForwardState : JumpingState {

        public JumpingForwardState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
        }
    }
}