﻿//using Assets.Scripts.Core;
//using UnityEngine;

//namespace Assets.Scripts.Kula.States {
//    public class GroundedState : StateBase {
//        private float distanceTraveled;
//        private Quaternion startHullRotation;

//        public GroundedState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
//        }

//        public override void Enter() {
//            base.Enter();

//            // reset the distance traveled
//            distanceTraveled = 0f;

//            // save current hull rotation
//            startHullRotation = KulaController.Hull.localRotation;
//        }

//        public override void Update() {
//            base.Update();
//        }

//        public override void FixedUpdate() {
//            base.FixedUpdate();

//            // update the distance traveled
//            distanceTraveled += Rigidbody.velocity.magnitude * Time.fixedDeltaTime;

//            // update hull rotation
//            float circumference = TweakData.BALL_DIAMETER * Mathf.PI;
//            float rotationAngle = (360f * distanceTraveled) / circumference;
//            KulaController.Hull.transform.rotation = startHullRotation * Quaternion.Euler(rotationAngle, 0f, 0f);
//        }

//        public override void Exit() {
//            base.Exit();
//        }
//    }
//}