﻿using Assets.Scripts.Core;

namespace Assets.Scripts.Kula.States {

    public class JumpingUpState : JumpingState {

        public JumpingUpState(KulaController kulaController, StateMachine stateMachine) : base(kulaController, stateMachine) {
        }
    }
}