﻿namespace Assets.Scripts {

    internal class TweakData {
        public const float BALL_DIAMETER = 0.6366197f;
        public const float BALL_OFFSET = -0.1816901f;
    }
}