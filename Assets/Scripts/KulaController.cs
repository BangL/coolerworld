using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts {

    public class KulaController : MonoBehaviour {
        // =====

        public Transform hull;
        public LayerMask groundCheckLayerMask;
        public float turnTime;
        public float rollTime;
        public float jumpTime;
        public float jumpForwardTimeframe;
        public float jumpHeight;
        public float gravityRatio;
        public bool useFixedTime;

        // =====

        private const float BALL_OFFSET = -0.1816901f;
        private const float JUMP_DISTANCE = 2f;

        // =====

        private Coroutine movementCoroutine;
        private float movementStartedAt;
        private bool isRolling = false;
        private bool isJumping = false;

        private Vector3 gravityForce = -Vector3.up;

        private Rigidbody rb;

        // =====

        private void Start() {
            rb = GetComponent<Rigidbody>();

            KulaInput kulaInput = new();
            kulaInput.KulaControls.TurnLeft.performed += OnTurnLeft;
            kulaInput.KulaControls.TurnRight.performed += OnTurnRight;
            kulaInput.KulaControls.RollForward.performed += OnRollForward;
            kulaInput.KulaControls.Jump.performed += OnJump;
            kulaInput.Enable();
        }

        private void Update() {
            if (!useFixedTime) InternalUpdate();
        }

        private void FixedUpdate() {
            if (useFixedTime) InternalUpdate();
        }

        private void InternalUpdate() {
            var gravity = -transform.up;
            // if (gravityForce != gravity) // gravity shift
            gravityForce = gravity;

            // apply gravity
            if (movementCoroutine == null)
                rb.AddForce(gravityForce * gravityRatio, ForceMode.Acceleration);
        }

        #region Turn

        private void OnTurnLeft(InputAction.CallbackContext context) {
            if (movementCoroutine != null || !IsGround()) return;
            StartAnim(TurnAnim(-90f));
        }

        private void OnTurnRight(InputAction.CallbackContext context) {
            if (movementCoroutine != null || !IsGround()) return;
            StartAnim(TurnAnim(90f));
        }

        private IEnumerator TurnAnim(float yAngle) {
            var t = 0f;

            var startRotation = transform.rotation;
            var targetRotation = (startRotation * Quaternion.Euler(0, yAngle, 0)).Round();
            var hullStartRotation = hull.localRotation.Round();
            while (t <= 1f) {
                t += (useFixedTime ? Time.fixedDeltaTime : Time.deltaTime) / turnTime;
                transform.rotation = Quaternion.Slerp(startRotation, targetRotation, t);
                hull.localRotation = hullStartRotation;
                yield return null;
            }
            transform.rotation = targetRotation;
            hull.localRotation = hullStartRotation;

            StopAnim();
        }

        #endregion Turn

        #region Roll

        private void OnRollForward(InputAction.CallbackContext context) {
            if (movementCoroutine != null && (!isJumping || !InForwardJumpTimeFrame()))
                return;

            if (movementCoroutine != null && isJumping)
                StartAnim(JumpForward(), true);
            else if (movementCoroutine == null && !isJumping && IsGround())
                StartAnim(RollForward());
        }

        private IEnumerator RollForward() {
            isRolling = true;
            var t = 0f;
            var rt = 0f;

            // position
            var startPosition = transform.position;
            var targetPosition = (startPosition + transform.forward).Round() + BALL_OFFSET * transform.up;

            // ground check
            var leftPosition = (startPosition - transform.right).Round();
            var rightPosition = (startPosition + transform.right).Round();
            var selfGround = IsGround();
            var fwdGround = IsGround(targetPosition);
            var isLeftGround = IsGround(leftPosition);
            var isRightGround = IsGround(rightPosition);
            var wantGravityShift = (selfGround && !fwdGround);
            var canGravityShift = (selfGround && !fwdGround && !isLeftGround && !isRightGround);

            if (!wantGravityShift || canGravityShift) {
                // hull rotation
                var startHullRotation = hull.localRotation;
                Quaternion targetHullRotation = (startHullRotation * Quaternion.Euler(startHullRotation.eulerAngles.x <= 180f ? -180f : 180f, 0f, 0f));

                while (t <= 1f) {
                    t += (useFixedTime ? Time.fixedDeltaTime : Time.deltaTime) / rollTime;

                    // roll
                    transform.position = Vector3.Lerp(startPosition, targetPosition, t);
                    hull.localRotation = Quaternion.Lerp(startHullRotation, targetHullRotation, t);

                    yield return null;

                    // gravity shift
                    if (canGravityShift && t >= 0.5f && rt == 0f) {
                        // body rotation
                        var startBodyRotation = transform.rotation;
                        var targetBodyRotation = (startBodyRotation * Quaternion.Euler(90f, 0f, 0f)).Round();

                        // calc positioning for gravity shift
                        var gsStartPos = transform.position;
                        var gsTargetPos = (targetPosition.Round() - (transform.up / 2)) + (BALL_OFFSET * transform.forward);
                        var gsCenter = (transform.position - (BALL_OFFSET * transform.up)) - (transform.up / 2);

                        var gravityShiftTime = (TweakData.BALL_DIAMETER * Mathf.PI / 4) * rollTime;
                        while (rt <= 1f) {
                            rt += (useFixedTime ? Time.fixedDeltaTime : Time.deltaTime) / gravityShiftTime;
                            transform.rotation = Quaternion.Lerp(startBodyRotation, targetBodyRotation, rt);

                            // curved center positioning of outer ball
                            transform.position = Vector3.Slerp(gsStartPos - gsCenter, gsTargetPos - gsCenter, rt) + gsCenter;

                            yield return null;
                        }
                        transform.rotation = targetBodyRotation;
                        transform.position = gsTargetPos;

                        // recalc positioning for roll phase 2
                        startPosition = targetPosition.Round() + BALL_OFFSET * transform.up;
                        targetPosition = (startPosition + transform.forward).Round() + BALL_OFFSET * transform.up;
                    }
                }

                transform.position = targetPosition;
                hull.localRotation = targetHullRotation;
            }

            isRolling = false;
            StopAnim();
        }

        #endregion Roll

        private void OnJump(InputAction.CallbackContext context) {
            if (movementCoroutine != null && (!isRolling || !InForwardJumpTimeFrame()))
                return;

            if (movementCoroutine != null && isRolling)
                StartAnim(JumpForward(), true);
            else if (movementCoroutine == null && !isRolling && IsGround())
                StartAnim(JumpUp());
        }

        private IEnumerator JumpUp() {
            isJumping = true;
            var t = 0f;

            var startPosition = transform.position;
            var peakPosition = (transform.position + transform.up * jumpHeight).Round();
            peakPosition += BALL_OFFSET * transform.up;

            while (t <= 1f) {
                t += (useFixedTime ? Time.fixedDeltaTime : Time.deltaTime) / jumpTime;
                transform.position = t < .5f ?
                    Vector3.Lerp(startPosition, peakPosition, EaseOutCubic(t * 2)) :
                    Vector3.Lerp(peakPosition, startPosition, EaseInCubic((t - .5f) * 2));

                yield return null;
            }
            transform.position = startPosition;

            isJumping = false;
            StopAnim();
        }

        private IEnumerator JumpForward() {
            isRolling = true;
            isJumping = true;
            var t = 0f;

            var startPosition = transform.position;
            var peakPosition = (transform.position + (transform.up * jumpHeight) + (transform.forward * (JUMP_DISTANCE / 2))).Round();
            var targetPosition = (transform.position + (transform.forward * JUMP_DISTANCE)).Round();
            peakPosition += BALL_OFFSET * transform.up;
            targetPosition += BALL_OFFSET * transform.up;

            while (t <= 1f) {
                t += (useFixedTime ? Time.fixedDeltaTime : Time.deltaTime) / jumpTime;
                transform.position = t < .5f ?
                    Vector3.Lerp(startPosition, peakPosition, EaseOutCubic(t * 2)) :
                    Vector3.Lerp(peakPosition, targetPosition, EaseInCubic((t - .5f) * 2));

                yield return null;
            }
            transform.position = targetPosition;

            isJumping = false;
            isRolling = false;

            StopAnim();
        }

        #region Easing

        private static float EaseOutCubic(float x) {
            return 1 - Mathf.Pow(1 - x, 3);
        }

        private static float EaseInCubic(float x) {
            return x * x * x;
        }

        #endregion Easing

        #region Helper

        private bool InForwardJumpTimeFrame() {
            return movementStartedAt == 0 || ((useFixedTime ? Time.fixedTime : Time.time) - movementStartedAt) <= jumpForwardTimeframe;
        }

        private void StartAnim(IEnumerator coroutine, bool force = false) {
            if (movementCoroutine != null)
                if (force) StopCoroutine(movementCoroutine);
                else return;
            movementStartedAt = (useFixedTime ? Time.fixedTime : Time.time);
            rb.isKinematic = true;
            movementCoroutine = StartCoroutine(coroutine);
        }

        private void StopAnim() {
            rb.ResetInertiaTensor();
            rb.isKinematic = false;
            movementCoroutine = null;
        }

        private bool IsGround(Vector3? position = null) {
            return Physics.Raycast(position ?? transform.position, -transform.up, transform.localScale.x + 0.001f, groundCheckLayerMask);
        }

        #endregion Helper
    }
}