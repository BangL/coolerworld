﻿using Assets.Scripts.Kula;
using System;
using UnityEngine;

namespace Assets.Scripts.Core {

    public abstract class StateBase {
        protected KulaController KulaController;
        protected StateMachine StateMachine;
        protected KulaInput KulaInput;
        protected Rigidbody Rigidbody;

        protected StateBase(KulaController kulaController, StateMachine stateMachine) {
            KulaInput = new KulaInput();
            KulaController = kulaController;
            StateMachine = stateMachine;
            Rigidbody = kulaController.GetComponent<Rigidbody>();
        }

        public virtual void Enter() {
        }

        public virtual void Update() {
        }

        public virtual void FixedUpdate() {
        }

        public virtual void Exit() {
        }
    }
}