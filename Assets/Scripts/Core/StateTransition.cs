﻿using System;

namespace Assets.Scripts.Core {

    internal class StateTransition {
        public Func<bool> Condition { get; }
        public StateBase TargetState { get; }

        public StateTransition(Func<bool> condition, StateBase targetState) {
            Condition = condition;
            TargetState = targetState;
        }
    }
}