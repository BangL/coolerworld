﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Core {

    public class StateMachine {
        private readonly Dictionary<Type, List<StateTransition>> _transitions = new();
        private List<StateTransition> _currentTransitions = new();

        public StateBase CurrentState { get; private set; }

        public void Initialize(StateBase startingState) {
            CurrentState = startingState;
            _currentTransitions = _transitions[CurrentState.GetType()]; // FIXME
            startingState.Enter();
        }

        public void ChangeState(StateBase newState) {
            CurrentState.Exit();
            //_currentTransitions.Clear();

            CurrentState = newState;
            _currentTransitions = _transitions[CurrentState.GetType()]; // FIXME

            newState.Enter();
        }

        public void RegisterTransition(StateBase sourceState, StateBase targetState, Func<bool> condition) {
            var sourceType = sourceState.GetType();
            var transition = new StateTransition(condition, targetState);

            if (_transitions.ContainsKey(sourceType))
                _transitions[sourceType].Add(transition);
            else
                _transitions.Add(sourceType, new List<StateTransition> { transition });
        }

        public void OnUpdate() {
            var transition = _currentTransitions.FirstOrDefault(x => x.Condition());

            if (transition != null)
                ChangeState(transition.TargetState);
        }
    }
}